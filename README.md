# hotel-management-system

## Project description

This project is a terminal based hotel management . syste.

## Running the system

### Prerequisites

* If you don't have git you have to install it first.

* Java 15 and maven is needed to run the project, so make sure you have installed them.

### Running

1. Clone the repository through `http` or `ssh`

`git clone https://gitlab.com/Echolz/hotel-management-system.git`

2. Change your working directory to the directory of the project

`cd hotel-management-syste`

3. Make sure you have installed the maven dependencies

`mvn clean install`

3. Run the `run.sh` script

`./run.sh`

## How to start development

1. Make sure you are on the `master` branch

`git checkout master`

2. Make sure you have the latest changes on master

`git pull`

3. Checkout a branch for your feature by running:

`git checkout -b your-branch-name`
    
* Make sure the branch name does not contain capital letters.

* Every word in your branch name should be separated with a dash.

* For example if implementing the service for the hotel rooms the branch name should be `hotel-rooms-service`

4. You can check the changes you have made by running:

`git status`

5. You can stage your files for a commit by running:

`git add .`

* This will add all the files you have changed. You can add files separately, refer to `git add` command documentation.

6. Commit your changes.

`git commit -m "Your commit message stands here"`

* Every commit message starts with a capital letter

* Commits don't end with a punctuation mark

* Commit messages should above 15 words in length and should describe your changes

* Commit messages like `git commit -m "fix a bug and refactor"` are not acceptable and should not pass the merge request process

* You can change your commit messages with `git commit --amend` and `git rebase`. Refer to the git documentation.

* Example commits messages:

```
Change internationalization cache name
Schedule spring tasks to clear timezones cache
```
7. Push your changes to an upstream branch

`git push -u origin your-branch-name`

* The `-u origin your-branch-name` will be needed only once. It sets the upstream branch of your local branch.

* After the first push after a checkout you can run `git push`.

8. Only steps 4-7 will be needed for the rest of the changes you want to push on your upstream branch.