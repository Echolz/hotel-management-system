package com.fmi.hotel.management.system.server.dao.interfaces;

import com.fmi.hotel.management.system.server.model.Feedback;
import com.fmi.hotel.management.system.server.model.Modelled;

public interface FeedbackDAO extends CrudDAO<Feedback>, Modelled<Feedback> {

}