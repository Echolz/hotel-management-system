package com.fmi.hotel.management.system.server.services;

import com.fmi.hotel.management.system.server.dao.interfaces.CrudDAO;
import com.fmi.hotel.management.system.server.model.Request;

public class RequestsService {
    private final CrudDAO<Request> requestsDAO;

    public RequestsService(CrudDAO<Request> requestsDAO) {
        this.requestsDAO = requestsDAO;
    }
}
