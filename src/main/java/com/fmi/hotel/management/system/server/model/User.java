package com.fmi.hotel.management.system.server.model;

import java.time.LocalDate;
import java.util.Objects;

public class User implements Model {

    private final Integer userID;
    private final String userName;
    private final String password;
    private final String email;
    private final String firstName;
    private final String lastName;
    private final LocalDate birthDate;
    private final String address;
    private final String personalIdentificationNumber;
    private final UserRole userRole;

    public User(Integer userID, String userName, String password, String email, String firstName, String lastName, LocalDate birthDate, String address, String personalIdentificationNumber, UserRole userRole) {
        this.userID = userID;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.address = address;
        this.personalIdentificationNumber = personalIdentificationNumber;
        this.userRole = userRole;
    }

    public User(Integer userID, User user) {
        this.userID = userID;
        this.userName = user.getUserName();
        this.password = user.getPassword();
        this.email = user.getEmail();
        this.firstName = user.getEmail();
        this.lastName = user.getLastName();
        this.birthDate = user.getBirthDate();
        this.address = user.getAddress();
        this.personalIdentificationNumber = user.getPersonalIdentificationNumber();
        this.userRole = user.getUserRole();
    }

    public Integer getID() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getAddress() {
        return address;
    }

    public String getPersonalIdentificationNumber() {
        return personalIdentificationNumber;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", personalIdentificationNumber='" + personalIdentificationNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userName, user.userName) &&
                Objects.equals(password, user.password) &&
                Objects.equals(email, user.email) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(birthDate, user.birthDate) &&
                Objects.equals(address, user.address) &&
                Objects.equals(personalIdentificationNumber, user.personalIdentificationNumber) &&
                Objects.equals(userRole, user.userRole);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, password, email, firstName, lastName, birthDate, address, personalIdentificationNumber, userRole);
    }
}
