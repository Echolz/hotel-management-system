package com.fmi.hotel.management.system.server.features.authorization;

import com.fmi.hotel.management.system.server.features.core.TechnicianFeature;

public class TechnicianAuthorizationFeature
{

	private final TechnicianFeature technicianFeature;

	public TechnicianAuthorizationFeature(TechnicianFeature technicianFeature)
	{
		this.technicianFeature = technicianFeature;
	}
}
