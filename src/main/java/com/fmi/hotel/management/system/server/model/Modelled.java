package com.fmi.hotel.management.system.server.model;

import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;

public interface Modelled<T> {
    T createModel(int id, T object) throws DAOException;
}
