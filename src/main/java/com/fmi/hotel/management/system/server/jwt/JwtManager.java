package com.fmi.hotel.management.system.server.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

public class JwtManager implements JwtTokenParserer, JwtTokenGenerator
{

	// The secret key. This should be in a property file NOT under source
	// control and not hard coded in real life. We're putting it here for
	// simplicity.
	private final String secreteKey;

	public JwtManager(String secreteKey)
	{
		this.secreteKey = secreteKey;
	}

	public String createJWT(String id, String issuer, String subject)
	{

		//The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		//We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secreteKey);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		//Let's set the JWT Claims
		JwtBuilder builder = Jwts.builder().setId(id)
								 .setIssuedAt(now)
								 .setSubject(subject)
								 .setIssuer(issuer)
								 .signWith(signatureAlgorithm, signingKey);

		//Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}

	public Claims decodeJWT(String jwt)
	{
		//This line will throw an exception if it is not a signed JWS (as expected)
		return Jwts.parser()
				   .setSigningKey(DatatypeConverter.parseBase64Binary(secreteKey))
				   .parseClaimsJws(jwt).getBody();
	}
}
