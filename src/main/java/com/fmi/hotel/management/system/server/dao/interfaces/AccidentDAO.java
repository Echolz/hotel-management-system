package com.fmi.hotel.management.system.server.dao.interfaces;

import com.fmi.hotel.management.system.server.model.Accident;
import com.fmi.hotel.management.system.server.model.Modelled;

public interface AccidentDAO extends CrudDAO<Accident>, Modelled<Accident> {

}