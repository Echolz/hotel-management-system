package com.fmi.hotel.management.system.server.dao.in_memory;

import com.fmi.hotel.management.system.server.dao.interfaces.FinancesDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.Payment;
import com.fmi.hotel.management.system.server.utilities.AutoIncrementer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class InMemoryFinancesDAO implements FinancesDAO {

    private final Map<Integer, Payment> payments;
    private final AutoIncrementer IDGenerator;

    public InMemoryFinancesDAO() {
        payments = new HashMap<>();
        IDGenerator = new AutoIncrementer();
    }

    @Override
    public int insert(Payment paymentToBeInserted) throws DAOException {
        if (paymentToBeInserted == null) {
            throw new DAOException("Cannot insert a NULL payment", this.getClass());
        }
        int newPaymentID = IDGenerator.generateID();
        Payment payment = new Payment(newPaymentID, paymentToBeInserted);
        payments.put(newPaymentID, payment);
        return newPaymentID;
    }

    @Override
    public void update(Payment paymentToUpdate) throws DAOException {
        if (paymentToUpdate == null) {
            throw new DAOException("Cannot update a NULL payment", this.getClass());
        }
        if (paymentToUpdate.getID() == null) {
            throw new DAOException("Cannot update a payment with NULL ID", this.getClass());
        }

        payments.put(paymentToUpdate.getID(), paymentToUpdate);
    }

    @Override
    public void deleteByID(int ID) throws DAOException {
        if (!payments.containsKey(ID)) {
            throw new DAOException("Can't delete a non-existent payment", this.getClass());
        }
        payments.remove(ID);
    }

    @Override
    public List<Payment> findAll() throws DAOException {
        return new ArrayList<Payment>(payments.values());
    }

    @Override
    public Payment findByID(int ID) throws DAOException {
        return payments.get(ID);
    }

    @Override
    public List<Payment> findAllByCriteria(Predicate<Payment> criteria) throws DAOException {
        if (criteria == null) {
            return findAll();
        }

        ArrayList<Payment> result = new ArrayList<>();
        result.ensureCapacity(payments.size());

        for (Payment currentPayment : payments.values()) {
            if (criteria.test(currentPayment)) {
                result.add(currentPayment);
            }
        }
        result.trimToSize();
        return result;
    }

    @Override
    public Payment createModel(int id, Payment object) {
        return new Payment(id, object);
    }
}

