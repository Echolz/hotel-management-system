package com.fmi.hotel.management.system.server.model;

import java.util.Objects;

public final class Accident implements Model {
    private final String name;
    private final int years;
    private final Integer ID;
    private final int doctorID;
    private final String telephoneNumber;
    private final String healthCondition;
    private final String drugs;
    private final String diagnosis;
    private final String treatment;

    public Accident(Integer ID, int doctorID, String name, int years, String telephoneNumber,
                    String healthCondition, String drugs, String diagnosis, String treatment) {
        this.ID = ID;
        this.doctorID = doctorID;
        this.years = years;
        this.name = Objects.requireNonNull(name);
        this.telephoneNumber = Objects.requireNonNull(telephoneNumber);
        this.healthCondition = Objects.requireNonNull(healthCondition);
        this.drugs = Objects.requireNonNull(drugs);
        this.diagnosis = Objects.requireNonNull(diagnosis);
        this.treatment = Objects.requireNonNull(treatment);
    }

    public Accident(int ID, Accident accident) {
        this(ID, accident.doctorID, accident.name, accident.years, accident.telephoneNumber,
                accident.healthCondition, accident.drugs, accident.diagnosis, accident.treatment);
    }

    public Accident(Accident accident) {
        this(accident.ID, accident);
    }

    public String getName() {
        return name;
    }

    public int getYears() {
        return years;
    }

    public Integer getID() {
        return ID;
    }

    public int getDoctorID() {
        return doctorID;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public String getHealthCondition() {
        return healthCondition;
    }

    public String getDrugs() {
        return drugs;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getTreatment() {
        return treatment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Accident accident = (Accident) o;
        return years == accident.years &&
                doctorID == accident.doctorID &&
                Objects.equals(name, accident.name) &&
                Objects.equals(telephoneNumber, accident.telephoneNumber) &&
                Objects.equals(healthCondition, accident.healthCondition) &&
                Objects.equals(drugs, accident.drugs) &&
                Objects.equals(diagnosis, accident.diagnosis) &&
                Objects.equals(treatment, accident.treatment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, years, doctorID, telephoneNumber, healthCondition, drugs, diagnosis, treatment);
    }
}
