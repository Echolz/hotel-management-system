package com.fmi.hotel.management.system.server.services;

import com.fmi.hotel.management.system.server.dao.interfaces.CrudDAO;
import com.fmi.hotel.management.system.server.model.Payment;

public class FinancesService {

    private final CrudDAO<Payment> financesDAO;

    public FinancesService(CrudDAO<Payment> financesDAO) {
        this.financesDAO = financesDAO;
    }

    // Declare the service methods when we define the API
}
