package com.fmi.hotel.management.system.server.dao.in_memory;

import com.fmi.hotel.management.system.server.dao.interfaces.UsersDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.User;
import com.fmi.hotel.management.system.server.utilities.AutoIncrementer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class InMemoryUsersDAO implements UsersDAO {

    private final Map<Integer, User> users;
    private final AutoIncrementer IDGenerator;

    public InMemoryUsersDAO() {
        this.users = new HashMap<>();
        this.IDGenerator = new AutoIncrementer();
    }

    @Override
    public User findByID(int ID) throws DAOException {
        return users.get(ID);
    }

    @Override
    public List<User> findAll() throws DAOException {
        return new ArrayList<>(users.values());
    }

    @Override
    public List<User> findAllByCriteria(Predicate<User> criteria) throws DAOException {
        if (criteria == null) {
            return findAll();
        }
        return users.values().stream().filter(criteria).collect(Collectors.toList());
    }

    @Override
    public int insert(User user) throws DAOException {
        if(user == null) {
            throw new DAOException("Can't insert a NULL user", this.getClass());
        }
        int newUserID = IDGenerator.generateID();
        User newUser = new User(newUserID, user);

        // room with the same number is already present
        if (userHasDuplicate(user)) {
            throw new DAOException(String.format("Can't add duplicate user, the user with info %s already exists", user.toString()), this.getClass());
        }

        users.put(newUserID, newUser);
        return newUserID;
    }


    @Override
    public void update(User user) throws DAOException {
        if(user == null) {
            throw new DAOException("Can't update NULL user", this.getClass());
        }

        if(user.getID() == null) {
            throw new DAOException("Can't update user with NULL ID", this.getClass());
        }
        // TODO add checks for valid user
        users.put(user.getID(), user);
    }

    @Override
    public void deleteByID(int ID) throws DAOException {
        // TODO add checks for valid user
        User deletedUser = users.remove(ID);

        if (deletedUser == null) {
            throw new DAOException(String.format("Can't delete a non-existing user with ID %d", ID), this.getClass());
        }
    }

    // checks if a user with the same username, email or national identification number already exists
    private boolean userHasDuplicate(User user) throws DAOException {
        List<User> users = findAllByCriteria(x -> x.getUserName().equals(user.getUserName())
                || x.getEmail().equals(user.getEmail())
                || x.getPersonalIdentificationNumber().equals(user.getPersonalIdentificationNumber()));

        return users.size() != 0;
    }

    @Override
    public User createModel(int id, User object) throws DAOException {
        return new User(id, object);
    }
}
