package com.fmi.hotel.management.system.server.services;

import com.fmi.hotel.management.system.server.dao.interfaces.CrudDAO;
import com.fmi.hotel.management.system.server.model.User;

public class UsersService {

    private final CrudDAO<User> usersCrudDAO;

    public UsersService(CrudDAO<User> usersCrudDAO) {
        this.usersCrudDAO = usersCrudDAO;
    }
}
