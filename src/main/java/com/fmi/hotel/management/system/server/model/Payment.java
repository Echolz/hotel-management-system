package com.fmi.hotel.management.system.server.model;

import java.time.LocalDate;
import java.util.Objects;

public final class Payment implements Model {

    private final Integer ID;
    private final Type type;
    private final long amount;
    private final String description;
    private final LocalDate createdAt;
    private final Integer payerID;

    public enum Type {
        SALARY,
        ROOM,
        WAREHOUSE
    }

    public Payment(Integer ID, Type type, long amount, String description, LocalDate createdAt, Integer payerID) {
        this.ID = ID;
        this.type = type;
        this.amount = amount;
        this.description = description;
        this.createdAt = createdAt;
        this.payerID = payerID;
    }

    public Payment(Integer ID, Payment payment) {
        this(ID, payment.type, payment.amount, payment.description, payment.createdAt, payment.payerID);
    }

    public Payment(Payment payment) {
        this(payment.ID, payment);
    }

    public Integer getID() {
        return ID;
    }

    public Type getType() {
        return type;
    }

    public long getAmount() {
        return amount;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return createdAt;
    }

    public Integer getPayerID() {
        return payerID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return amount == payment.amount &&
                Objects.equals(type, payment.type) &&
                Objects.equals(description, payment.description) &&
                Objects.equals(createdAt, payment.createdAt) &&
                Objects.equals(payerID, payment.payerID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, amount, description, createdAt, payerID);
    }
}