package com.fmi.hotel.management.system.server.model;

import com.fmi.hotel.management.system.server.model.Model;

import java.time.LocalDateTime;
import java.util.Objects;


public final class Feedback implements Model {

    private final Integer ID;
    private final int userID;
    private final Rating rating;
    private final String comment;
    private final LocalDateTime createdAt;

    public enum Rating {
        AWFUL,
        BAD,
        AVERAGE,
        GOOD,
        EXCELLENT,
    }

    public Feedback(Integer ID, int userID, Rating rating, String comment, LocalDateTime createdAt) {
        this.ID = ID;
        this.userID = userID;
        this.rating = Objects.requireNonNull(rating);
        this.comment = Objects.requireNonNull(comment);
        this.createdAt = Objects.requireNonNull(createdAt);
    }

    public Feedback(int ID, Feedback feedback) {
        this(ID, feedback.userID, feedback.rating, feedback.comment, feedback.createdAt);
    }

    public Feedback(Feedback feedback) {
        this(feedback.ID, feedback);
    }


    @Override
    public Integer getID() {
        return ID;
    }

    public int getUserID() {
        return userID;
    }

    public Rating getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Feedback feedback = (Feedback) o;

        return userID == feedback.userID &&
                rating == feedback.rating &&
                comment.equals(feedback.comment) &&
                createdAt.equals(feedback.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userID, rating, comment, createdAt);
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "ID=" + ID +
                ", userID=" + userID +
                ", rating=" + rating +
                ", comment='" + comment + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
