package com.fmi.hotel.management.system.server.dao.interfaces;

import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.Model;

import java.util.List;
import java.util.function.Predicate;

public interface CrudDAO<T extends Model> {
    T findByID(int ID) throws DAOException;

    List<T> findAll() throws DAOException;

    List<T> findAllByCriteria(Predicate<T> criteria) throws DAOException;

    int insert(T object) throws DAOException;

    void update(T object) throws DAOException;

    void deleteByID(int ID) throws DAOException;
}