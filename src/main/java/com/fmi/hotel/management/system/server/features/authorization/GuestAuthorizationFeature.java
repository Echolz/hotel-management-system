package com.fmi.hotel.management.system.server.features.authorization;

import com.fmi.hotel.management.system.server.features.core.GuestFeature;

public class GuestAuthorizationFeature
{
	private final GuestFeature guestFeature;

	public GuestAuthorizationFeature(GuestFeature guestFeature)
	{
		this.guestFeature = guestFeature;
	}
}
