package com.fmi.hotel.management.system.server.model;

import java.time.LocalDateTime;
import java.util.Objects;

public final class Death implements Model {
    private final String name;
    private final int years;
    private final Integer ID;
    private final int doctorID;
    private final String causeOfDeath;
    private final LocalDateTime momentOfDeath;
    private final String deathCertificateNumber;

    public Death(Integer ID, int doctorID, String name, int years, String causeOfDeath, LocalDateTime momentOfDeath,
                 String deathCertificateNumber){
        this.ID = ID;
        this.doctorID = doctorID;
        this.years = years;
        this.name = Objects.requireNonNull(name);
        this.causeOfDeath = Objects.requireNonNull(causeOfDeath);
        this.momentOfDeath = Objects.requireNonNull(momentOfDeath);
        this.deathCertificateNumber = Objects.requireNonNull(deathCertificateNumber);
    }
    public Death(int ID, Death death){
        this(ID, death.doctorID, death.name, death.years, death.causeOfDeath, death.momentOfDeath, death.deathCertificateNumber);
    }

    public Death(Death death){ this(death.ID, death); }

    public String getName(){ return name; }

    public int getYears(){ return years; }

    public Integer getID() {
        return ID;
    }

    public int getDoctorID() { return doctorID; }

    public String getCauseOfDeath() {
        return causeOfDeath;
    }

    public LocalDateTime getMomentOfDeath() {
        return momentOfDeath;
    }

    public String getDeathCertificateNumber() {
        return deathCertificateNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Death death = (Death) o;
        return years == death.years &&
                doctorID == death.doctorID &&
                Objects.equals(name, death.name) &&
                Objects.equals(causeOfDeath, death.causeOfDeath) &&
                Objects.equals(momentOfDeath, death.momentOfDeath) &&
                Objects.equals(deathCertificateNumber, death.deathCertificateNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, years, doctorID, causeOfDeath, momentOfDeath, deathCertificateNumber);
    }
}
