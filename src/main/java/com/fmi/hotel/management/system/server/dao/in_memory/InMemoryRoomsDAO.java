package com.fmi.hotel.management.system.server.dao.in_memory;

import com.fmi.hotel.management.system.server.dao.interfaces.RoomsDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.Room;
import com.fmi.hotel.management.system.server.utilities.AutoIncrementer;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class InMemoryRoomsDAO implements RoomsDAO {

    private final Map<Integer, Room> rooms;
    private final AutoIncrementer IDGenerator;

    public InMemoryRoomsDAO() {
        rooms = new HashMap<>();
        IDGenerator = new AutoIncrementer();
    }

    @Override
    public Room findByID(int ID) throws DAOException {
        return rooms.get(ID);
    }

    @Override
    public Room findByNumber(int number) throws DAOException {
        Optional<Room> optionalRoom = rooms.values().stream()
                .filter(room -> room.getNumber() == number)
                .findFirst();

        return optionalRoom.orElse(null);
    }

    @Override
    public List<Room> findAll() throws DAOException {
        return new ArrayList<>(rooms.values());
    }

    @Override
    public List<Room> findAllByCriteria(Predicate<Room> criteria) throws DAOException {
        return rooms.values().stream().filter(room -> (criteria == null || criteria.test(room))).collect(Collectors.toList());
    }

    @Override
    public int insert(Room room) throws DAOException {
        if (room == null) {
            throw new DAOException("Can't inert a NULL room", this.getClass());
        }
        int newRoomID = IDGenerator.generateID();
        Room newRoom = new Room(newRoomID, room);

        // room with the same number is already present
        if (findByNumber(room.getNumber()) != null) {
            throw new DAOException(String.format("Can't add duplicate room number %d", room.getNumber()), this.getClass());
        }

        rooms.put(newRoomID, newRoom);
        return newRoomID;
    }

    @Override
    public void update(Room room) throws DAOException {
        if (room == null) {
            throw new DAOException("Can't update a NULL room", this.getClass());
        }
        if (room.getID() == null) {
            throw new DAOException("Can't update a room with NULL ID", this.getClass());
        }
        if (!rooms.containsKey(room.getID())) {
            throw new DAOException(String.format("Can't update a non-existing room with ID %d", room.getID()), this.getClass());
        }
        if (rooms.values().stream().anyMatch(currentRoom -> (currentRoom.getID().equals(room.getID()) && currentRoom.getNumber() != room.getNumber()))) {
            throw new DAOException(String.format("Can't update the number of room %d from %d to %d",
                    room.getID(), findByID(room.getID()).getNumber(), room.getNumber()), this.getClass());
        }

        rooms.put(room.getID(), room);
    }

    @Override
    public void deleteByID(int ID) throws DAOException {
        if (!rooms.containsKey(ID)) {
            throw new DAOException(String.format("Can't delete a non-existing room with ID %d", ID), this.getClass());
        }
        if (rooms.get(ID).getOwnerID() != null) {
            throw new DAOException(String.format("Can't delete room with ID %d with guests", ID), this.getClass());
        }
        rooms.remove(ID);
    }

    @Override
    public Room createModel(int id, Room object) throws DAOException {
        return new Room(id, object);
    }
}
