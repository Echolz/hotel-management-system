package com.fmi.hotel.management.system.server.dao.in_memory;

import com.fmi.hotel.management.system.server.dao.interfaces.AccidentDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.Accident;
import com.fmi.hotel.management.system.server.utilities.AutoIncrementer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class InMemoryAccidentDAO implements AccidentDAO {

    private final List<Accident> accidents = new ArrayList<>();
    private final AutoIncrementer IDGenerator = new AutoIncrementer();

    private List<Accident> clone(List<Accident> source) {
        return source.stream()
                .map(Accident::new)
                .collect(Collectors.toList());
    }

    @Override
    public Accident findByID(int ID) throws DAOException {
        Accident filteredAccident = accidents.stream()
                .filter(accident -> accident.getID().equals(ID))
                .findFirst()
                .orElse(null);

        return filteredAccident == null ? null : new Accident(filteredAccident);
    }

    @Override
    public List<Accident> findAll() throws DAOException {
        return clone(accidents);
    }

    @Override
    public List<Accident> findAllByCriteria(Predicate<Accident> predicate) throws DAOException {
        if (predicate == null) {
            return findAll();
        }

        List<Accident> filteredAccidents = accidents.stream()
                .filter(predicate)
                .collect(Collectors.toList());

        return clone(filteredAccidents);
    }

    @Override
    public int insert(Accident accidentToBeInserted) throws DAOException {
        if (accidentToBeInserted == null) {
            throw new DAOException("Cannot insert NULL accident", this.getClass());
        }

        int generatedID = IDGenerator.generateID();
        accidents.add(new Accident(generatedID, accidentToBeInserted));

        return generatedID;
    }

    @Override
    public void update(Accident updatedAccident) throws DAOException {
        if (updatedAccident == null) {
            throw new DAOException("Cannot update NULL accident", this.getClass());
        }
        if (updatedAccident.getID() == null) {
            throw new DAOException("Cannot update accident with NULL ID", this.getClass());
        }
        boolean wasAccidentDeleted = accidents.removeIf(accident -> updatedAccident.getID().equals(accident.getID()));

        if (!wasAccidentDeleted) {
            throw new DAOException("Accident was not found in the system. " +
                    "Please make sure you're updating an existing record.", this.getClass());
        }

        accidents.add(new Accident(updatedAccident));
    }

    @Override
    public void deleteByID(int ID) throws DAOException {
        boolean wasAccidentDeleted = accidents.removeIf(accident -> accident.getID().equals(ID));

        if (!wasAccidentDeleted) {
            throw new DAOException("Accident was not found in the system. " +
                    "Please make sure you're deleting an existing record.", this.getClass());
        }
    }

    @Override
    public Accident createModel(int id, Accident object) {
        return new Accident(id, object);
    }
}
