package com.fmi.hotel.management.system.server.features.authorization;

import com.fmi.hotel.management.system.server.features.core.AccountantFeature;

public class AccountantAuthorizationFeature
{

	private final AccountantFeature accountantFeature;

	public AccountantAuthorizationFeature(AccountantFeature accountantFeature)
	{
		this.accountantFeature = accountantFeature;
	}
}
