package com.fmi.hotel.management.system.server.features.authorization;

import com.fmi.hotel.management.system.server.features.core.WarehousemanFeature;

public class WarehousemanAuthorizationFeature
{

	private final WarehousemanFeature warehousemanFeature;

	public WarehousemanAuthorizationFeature(WarehousemanFeature warehousemanFeature)
	{
		this.warehousemanFeature = warehousemanFeature;
	}
}
