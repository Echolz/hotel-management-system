package com.fmi.hotel.management.system.server.dao.interfaces;

import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.Modelled;
import com.fmi.hotel.management.system.server.model.Room;

public interface RoomsDAO extends CrudDAO<Room>, Modelled<Room> {
    public Room findByNumber(int ID) throws DAOException;
}