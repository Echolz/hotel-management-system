package com.fmi.hotel.management.system.server.model;

public enum UserRole {
    ACCOUNTANT, ADMINISTRATION, DOCTOR, GUEST, MANAGER, RECEPTIONIST, TECHNICIAN, WAREHOUSEMAN
}
