package com.fmi.hotel.management.system.server.dao.interfaces;

import com.fmi.hotel.management.system.server.model.Modelled;
import com.fmi.hotel.management.system.server.model.Request;

public interface RequestsDAO extends CrudDAO<Request>, Modelled<Request> {

}