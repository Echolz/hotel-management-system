package com.fmi.hotel.management.system.server.dao.interfaces;

import com.fmi.hotel.management.system.server.model.Modelled;
import com.fmi.hotel.management.system.server.model.Payment;

public interface FinancesDAO extends CrudDAO<Payment>, Modelled<Payment> {

}
