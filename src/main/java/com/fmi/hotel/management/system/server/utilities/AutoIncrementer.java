package com.fmi.hotel.management.system.server.utilities;

public class AutoIncrementer {

    private int ID;

    public AutoIncrementer() {
        ID = 0;
    }

    public int generateID() {
        ID++;
        return ID;
    }
}
