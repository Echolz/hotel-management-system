package com.fmi.hotel.management.system.server.features.authorization;

import com.fmi.hotel.management.system.server.features.core.ReceptionistFeature;

public class ReceptionistAuthorizationFeature
{
	private final ReceptionistFeature receptionistFeature;

	public ReceptionistAuthorizationFeature(ReceptionistFeature receptionistFeature)
	{
		this.receptionistFeature = receptionistFeature;
	}
}
