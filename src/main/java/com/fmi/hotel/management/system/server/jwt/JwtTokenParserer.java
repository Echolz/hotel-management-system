package com.fmi.hotel.management.system.server.jwt;

import io.jsonwebtoken.Claims;

public interface JwtTokenParserer
{

	public Claims decodeJWT(String jwt);
}
