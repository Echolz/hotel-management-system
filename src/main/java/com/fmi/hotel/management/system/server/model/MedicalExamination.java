package com.fmi.hotel.management.system.server.model;

import java.util.Objects;

public final class MedicalExamination implements Model {
    private final String name;
    private final int years;
    private final Integer ID;
    private final int doctorID;
    private final String telephoneNumber;
    private final String healthCondition;
    private final String prescribedDrugs;
    private final String otherDrugs;
    private final String diagnosis;
    private final String treatment;

    public MedicalExamination(Integer ID, int doctorID, String name, int years, String telephoneNumber, String healthCondition,
                              String prescribedDrugs, String otherDrugs, String diagnosis, String treatment) {
        this.ID = ID;
        this.doctorID = doctorID;
        this.years = years;
        this.name = Objects.requireNonNull(name);
        this.telephoneNumber = Objects.requireNonNull(telephoneNumber);
        this.healthCondition = Objects.requireNonNull(healthCondition);
        this.prescribedDrugs = Objects.requireNonNull(prescribedDrugs);
        this.otherDrugs = Objects.requireNonNull(otherDrugs);
        this.diagnosis = Objects.requireNonNull(diagnosis);
        this.treatment = Objects.requireNonNull(treatment);
    }

    public MedicalExamination(int ID, MedicalExamination medicalExamination){
        this(ID, medicalExamination.doctorID, medicalExamination.name,  medicalExamination.years, medicalExamination.telephoneNumber,
                medicalExamination.healthCondition, medicalExamination.prescribedDrugs, medicalExamination.otherDrugs,
                medicalExamination.diagnosis, medicalExamination.treatment);
    }

    public MedicalExamination(MedicalExamination medicalExamination){ this(medicalExamination.ID, medicalExamination); }

    public String getName(){
        return name;
    }

    public int getYears(){
        return years;
    }

    public Integer getID() {
        return ID;
    }

    public int getDoctorID() {
        return doctorID;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public String getHealthCondition() {
        return healthCondition;
    }

    public String getPrescribedDrugs() {
        return prescribedDrugs;
    }

    public String getOtherDrugs() {
        return otherDrugs;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getTreatment() {
        return treatment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicalExamination that = (MedicalExamination) o;
        return years == that.years &&
                doctorID == that.doctorID &&
                Objects.equals(name, that.name) &&
                Objects.equals(telephoneNumber, that.telephoneNumber) &&
                Objects.equals(healthCondition, that.healthCondition) &&
                Objects.equals(prescribedDrugs, that.prescribedDrugs) &&
                Objects.equals(otherDrugs, that.otherDrugs) &&
                Objects.equals(diagnosis, that.diagnosis) &&
                Objects.equals(treatment, that.treatment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, years, doctorID, telephoneNumber, healthCondition, prescribedDrugs, otherDrugs, diagnosis, treatment);
    }
}
