package com.fmi.hotel.management.system.server.dao.in_memory;

import com.fmi.hotel.management.system.server.dao.interfaces.MedicalExaminationDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.MedicalExamination;
import com.fmi.hotel.management.system.server.utilities.AutoIncrementer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class InMemoryMedicalExaminationDAO implements MedicalExaminationDAO {

    private final List<MedicalExamination> medicalExaminations = new ArrayList<>();
    private final AutoIncrementer IDGenerator = new AutoIncrementer();

    private List<MedicalExamination> clone(List<MedicalExamination> source) {
        return source.stream()
                .map(MedicalExamination::new)
                .collect(Collectors.toList());
    }

    @Override
    public MedicalExamination findByID(int ID) throws DAOException {
        MedicalExamination filteredMedicalExamination = medicalExaminations.stream()
                .filter(medicalExamination -> medicalExamination.getID().equals(ID))
                .findFirst()
                .orElse(null);

        return filteredMedicalExamination == null ? null : new MedicalExamination(filteredMedicalExamination);
    }

    @Override
    public List<MedicalExamination> findAll() throws DAOException {
        return clone(medicalExaminations);
    }

    @Override
    public List<MedicalExamination> findAllByCriteria(Predicate<MedicalExamination> predicate) throws DAOException {
        if (predicate == null) {
            return findAll();
        }

        List<MedicalExamination> filteredMedicalExaminations = medicalExaminations.stream()
                .filter(predicate)
                .collect(Collectors.toList());

        return clone(filteredMedicalExaminations);
    }

    @Override
    public int insert(MedicalExamination medicalExaminationToBeInserted) throws DAOException {
        if (medicalExaminationToBeInserted == null) {
            throw new DAOException("Cannot insert NULL medical examination", this.getClass());
        }

        int generatedID = IDGenerator.generateID();
        medicalExaminations.add(new MedicalExamination(generatedID, medicalExaminationToBeInserted));

        return generatedID;
    }

    @Override
    public void update(MedicalExamination updatedMedicalExamination) throws DAOException {
        if (updatedMedicalExamination == null) {
            throw new DAOException("Cannot update NULL medical examination", this.getClass());
        }
        if (updatedMedicalExamination.getID() == null) {
            throw new DAOException("Cannot update medical examination with NULL ID", this.getClass());
        }
        boolean wasMedicalExaminationDeleted = medicalExaminations.removeIf(examination -> updatedMedicalExamination.getID().equals(examination.getID()));

        if (!wasMedicalExaminationDeleted) {
            throw new DAOException("Medical examination was not found in the system. " +
                    "Please make sure you're updating an existing record.", getClass());
        }

        medicalExaminations.add(new MedicalExamination(updatedMedicalExamination));
    }

    @Override
    public void deleteByID(int ID) throws DAOException {
        boolean wasMedicalExaminationDeleted = medicalExaminations.removeIf(medicalExamination -> medicalExamination.getID().equals(ID));

        if (!wasMedicalExaminationDeleted) {
            throw new DAOException("Medical examination was not found in the system. " +
                    "Please make sure you're deleting an existing record.", this.getClass());
        }
    }

    @Override
    public MedicalExamination createModel(int id, MedicalExamination object) {
        return new MedicalExamination(id, object);
    }
}
