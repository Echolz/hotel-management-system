package com.fmi.hotel.management.system.server.dao.in_memory;

import com.fmi.hotel.management.system.server.dao.interfaces.RequestsDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.Category;
import com.fmi.hotel.management.system.server.model.Request;
import com.fmi.hotel.management.system.server.utilities.AutoIncrementer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class InMemoryRequestsDAO implements RequestsDAO {

    // ___________________________________
    // |  --key--  |      --value--      |
    // |-----------|---------------------|
    // |           |   -key-   | -value- |
    // |  category |---------------------|
    // |           |    ID     | request |
    // |___________|___________|_________|

    private final Map<Category, Map<Integer, Request>> allRequests;
    private final AutoIncrementer incrementer;

    public InMemoryRequestsDAO() {
        allRequests = new HashMap<>();
        incrementer = new AutoIncrementer();

        for (Category category : Category.values()) {
            Map<Integer, Request> categoryRequests = new HashMap<>();
            allRequests.put(category, categoryRequests);
        }
    }

    @Override
    public int insert(Request request) throws DAOException {
        if (request == null) {
            throw new DAOException("Inserting a NULL request is not allowed.", getClass());
        }

        Category category = request.getCategory();
        int generatedID = incrementer.generateID();
        Request modifiedRequest = new Request(generatedID, request);

        allRequests.get(category).put(generatedID, modifiedRequest);

        return generatedID;
    }

    @Override
    public List<Request> findAll() {
        return allCategoriesToList();
    }

    @Override
    public List<Request> findAllByCriteria(Predicate<Request> criteria) {
        return allRequests.values()
                .stream()
                .flatMap(innerMap -> innerMap.values().stream())
                .filter(currentRequest -> (criteria == null || criteria.test(currentRequest)))
                .map(currentRequest -> new Request(currentRequest.getID(), currentRequest))
                .collect(Collectors.toList());
    }

    @Override
    public Request findByID(int ID) throws DAOException {
        for (Category c : allRequests.keySet()) {
            for (Integer i : allRequests.get(c).keySet()) {
                if (i == ID) {
                    return allRequests.get(c).get(i);
                }
            }
        }

        return null;
    }

    @Override
    public void update(Request request) throws DAOException {
        if (!isValidRequest(request)) {
            throw new DAOException("Updating a NULL request is not allowed.", getClass());
        }

        Integer ID = request.getID();
        Category category = request.getCategory();

        //the ID does not exist in any of the nested maps
        if (allRequests.values().stream().noneMatch(innerMap -> innerMap.containsKey(ID))) {
            throw new DAOException("Can't update a Request that does not exist.", getClass());
        }

        //delete the old Request
        allRequests.values().forEach(innerMap -> innerMap.remove(ID));

        allRequests.get(category).put(ID, request);
    }

    @Override
    public void deleteByID(int ID) throws DAOException {
        for (Category c : allRequests.keySet()) {
            for (Integer i : allRequests.get(c).keySet()) {
                if (i == ID) {
                    allRequests.get(c).remove(i);
                    return;
                }
            }
        }

        throw new DAOException("Invalid ID", getClass());
    }

    private List<Request> allCategoriesToList() {
        List<Request> list = new ArrayList<>();

        for (Category c : allRequests.keySet()) {
            list.addAll(categoryToList(c));
        }

        return list;
    }

    private List<Request> categoryToList(Category category) {
        return new ArrayList<Request>(allRequests.get(category).values());
    }

    private boolean isValidRequest(Request request) {
        return request != null;
    }

    @Override
    public Request createModel(int id, Request object) {
        return new Request(id, object);
    }
}
