package com.fmi.hotel.management.system.server.features.core;

import com.fmi.hotel.management.system.server.jwt.JwtTokenGenerator;

public class LoginFeature
{

	private final JwtTokenGenerator jwtTokenGenerator;

	public LoginFeature(JwtTokenGenerator jwtTokenGenerator)
	{
		this.jwtTokenGenerator = jwtTokenGenerator;
	}
}
