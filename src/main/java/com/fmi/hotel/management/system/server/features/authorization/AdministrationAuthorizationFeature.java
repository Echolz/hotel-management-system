package com.fmi.hotel.management.system.server.features.authorization;

import com.fmi.hotel.management.system.server.features.core.AdministrationFeature;

public class AdministrationAuthorizationFeature
{
	private final AdministrationFeature administrationFeature;

	public AdministrationAuthorizationFeature(AdministrationFeature administrationFeature)
	{
		this.administrationFeature = administrationFeature;
	}
}
