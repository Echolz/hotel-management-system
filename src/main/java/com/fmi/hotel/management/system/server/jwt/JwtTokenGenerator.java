package com.fmi.hotel.management.system.server.jwt;

public interface JwtTokenGenerator
{

	public String createJWT(String id, String issuer, String subject);
}
