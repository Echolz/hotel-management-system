package com.fmi.hotel.management.system.server.features.authorization;

import com.fmi.hotel.management.system.server.features.core.DoctorFeature;

public class DoctorAuthorizationFeature
{

	private final DoctorFeature doctorFeature;

	public DoctorAuthorizationFeature(DoctorFeature doctorFeature)
	{
		this.doctorFeature = doctorFeature;
	}
}
