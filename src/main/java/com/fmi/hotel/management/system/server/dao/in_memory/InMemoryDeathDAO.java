package com.fmi.hotel.management.system.server.dao.in_memory;

import com.fmi.hotel.management.system.server.dao.interfaces.DeathDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.Death;
import com.fmi.hotel.management.system.server.utilities.AutoIncrementer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class InMemoryDeathDAO implements DeathDAO {

    private final List<Death> deaths = new ArrayList<>();
    private final AutoIncrementer IDGenerator = new AutoIncrementer();

    private List<Death> clone(List<Death> source) {
        return source.stream()
                .map(Death::new)
                .collect(Collectors.toList());
    }

    @Override
    public Death findByID(int ID) throws DAOException {
        Death filteredDeath = deaths.stream()
                .filter(death -> death.getID().equals(ID))
                .findFirst()
                .orElse(null);

        return filteredDeath == null ? null : new Death(filteredDeath);
    }

    @Override
    public List<Death> findAll() throws DAOException {
        return clone(deaths);
    }

    @Override
    public List<Death> findAllByCriteria(Predicate<Death> predicate) throws DAOException {
        if (predicate == null) {
            return findAll();
        }

        List<Death> filteredDeaths = deaths.stream()
                .filter(predicate)
                .collect(Collectors.toList());

        return clone(filteredDeaths);
    }

    @Override
    public int insert(Death deathToBeInserted) throws DAOException {
        if (deathToBeInserted == null) {
            throw new DAOException("Cannot insert NULL death", this.getClass());
        }

        int generatedID = IDGenerator.generateID();
        deaths.add(new Death(generatedID, deathToBeInserted));

        return generatedID;
    }

    @Override
    public void update(Death updatedDeath) throws DAOException {
        if (updatedDeath == null) {
            throw new DAOException("Cannot update NULL death", this.getClass());
        }
        if (updatedDeath.getID() == null) {
            throw new DAOException("Cannot update death with NULL ID", this.getClass());
        }
        boolean wasDeathDeleted = deaths.removeIf(death -> updatedDeath.getID().equals(death.getID()));

        if (!wasDeathDeleted) {
            throw new DAOException("Death was not found in the system. " +
                    "Please make sure you're updating an existing record.", this.getClass());
        }

        deaths.add(new Death(updatedDeath));
    }

    @Override
    public void deleteByID(int ID) throws DAOException {
        boolean wasDeathDeleted = deaths.removeIf(death -> death.getID().equals(ID));

        if (!wasDeathDeleted) {
            throw new DAOException("Death was not found in the system. " +
                    "Please make sure you're deleting an existing record.", this.getClass());
        }
    }

    @Override
    public Death createModel(int id, Death object) {
        return new Death(id, object);
    }
}
