package com.fmi.hotel.management.system.server.model;

import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Room implements Model {

    private final Integer roomID;
    private final int number;
    private final int bedCount;
    private final long pricePerNight; // in Bulgarian cents
    private final boolean hasTerrace;
    private final boolean isInRepairment;
    /*
        One room can have many guests, no duplicates,
        one user can be in multiple rooms as either the owner or an occupant
        TODO: write documentation for everything with javadoc
     */
    private final Set<Integer> occupantIDs;
    private final Integer ownerID;

    public Room(Integer roomID, int number, int bedCount, long pricePerNight, boolean hasTerrace, boolean isInRepairment, Set<Integer> occupantIDs, Integer ownerID) throws DAOException {
        if (roomID != null) {
            Room.validateID(roomID);
        }
        Room.validateNumber(number);
        Room.validateBedCount(bedCount);
        Room.validatePricePerNight(pricePerNight);
        Room.validateOccupants(ownerID, occupantIDs);

        this.roomID = roomID;
        this.number = number;
        this.bedCount = bedCount;
        this.pricePerNight = pricePerNight;
        this.hasTerrace = hasTerrace;
        this.isInRepairment = isInRepairment;
        if (occupantIDs != null) {
            this.occupantIDs = new HashSet<>(occupantIDs);
        } else {
            this.occupantIDs = null;
        }
        this.ownerID = ownerID;
    }

    public Room(int roomID, Room room) throws DAOException {
        if (room == null) {
            throw new DAOException("Room can't be null.", this.getClass());
        }
        Room.validateID(roomID);
        Room.validateNumber(room.number);
        Room.validateBedCount(room.bedCount);
        Room.validatePricePerNight(room.pricePerNight);
        Room.validateOccupants(room.ownerID, room.occupantIDs);

        this.roomID = roomID;
        this.number = room.number;
        this.bedCount = room.bedCount;
        this.pricePerNight = room.pricePerNight;
        this.hasTerrace = room.hasTerrace;
        this.isInRepairment = room.isInRepairment;
        if (room.occupantIDs != null) {
            this.occupantIDs = new HashSet<>(room.occupantIDs);
        } else {
            this.occupantIDs = null;
        }
        this.ownerID = room.ownerID;
    }

    public Integer getID() {
        return roomID;
    }

    public int getNumber() {
        return number;
    }

    public int getBedCount() {
        return bedCount;
    }

    public long getPricePerNight() {
        return pricePerNight;
    }

    public boolean hasTerrace() {
        return hasTerrace;
    }

    public boolean isInRepairment() {
        return isInRepairment;
    }

    public Set<Integer> getOccupantIDs() {
        if (occupantIDs == null) {
            return null;
        }
        return new HashSet<>(occupantIDs);
    }

    public Integer getOwnerID() {
        return ownerID;
    }

    @Override
    public String toString() {
        //When the console commands and output format are implemented, this will change
        return String.format("Room number: %d\nBeds: %d\nPrice: %d\nHas terrace: %b\nID: %d\n",
                number, bedCount, pricePerNight, hasTerrace, roomID);
    }

    //when User.validateID is created, this function will be removed
    public static void validateUserID(int ID) throws DAOException {
        if (ID <= 0) {
            throw new DAOException(String.format("User ID = %d is invalid - needs to be positive", ID), Room.class);
        }
    }

    public static void validateID(int ID) throws DAOException {
        if (ID <= 0) {
            throw new DAOException(String.format("Room ID = %d is invalid - needs to be positive", ID), Room.class);
        }
    }

    public static void validateNumber(int number) throws DAOException {
        if (number <= 0) {
            throw new DAOException(String.format("Room number = %d is invalid - needs to be positive", number), Room.class);
        }
    }

    public static void validateBedCount(int bedCount) throws DAOException {
        if (bedCount <= 0 || bedCount > 100) {
            throw new DAOException(String.format("Bed count = %d is invalid - needs to be positive and no more than 100", bedCount), Room.class);
        }
    }

    public static void validatePricePerNight(long price) throws DAOException {
        if (price <= 0) {
            throw new DAOException(String.format("Room price = %d is invalid - needs to be positive", price), Room.class);
        }
    }

    public static void validateOccupants(Integer ownerID, Set<Integer> occupantIDs) throws DAOException {
        if (occupantIDs == null) {
            throw new DAOException("Cannot have NULL occupants for an owner, use an empty set of IDs instead", Room.class);
        }
        if (ownerID == null) {
            if (occupantIDs.size() > 0) {
                throw new DAOException("Cannot have occupants without owner", Room.class);
            }
        } else {
            Room.validateUserID(ownerID);

            for (Integer ID : occupantIDs) {
                if (ID == null) {
                    throw new DAOException("Cannot have NULL ID in the occupants", Room.class);
                }
                Room.validateUserID(ID);
            }
            if (occupantIDs.contains(ownerID)) {
                throw new DAOException("Cannot have the owner as a part of the occupants", Room.class);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return number == room.number &&
                bedCount == room.bedCount &&
                pricePerNight == room.pricePerNight &&
                hasTerrace == room.hasTerrace &&
                isInRepairment == room.isInRepairment &&
                Objects.equals(occupantIDs, room.occupantIDs) &&
                Objects.equals(ownerID, room.ownerID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, bedCount, pricePerNight, hasTerrace, isInRepairment, occupantIDs, ownerID);
    }
}
