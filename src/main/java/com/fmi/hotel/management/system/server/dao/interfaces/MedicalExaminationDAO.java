package com.fmi.hotel.management.system.server.dao.interfaces;

import com.fmi.hotel.management.system.server.model.MedicalExamination;
import com.fmi.hotel.management.system.server.model.Modelled;

public interface MedicalExaminationDAO extends CrudDAO<MedicalExamination>, Modelled<MedicalExamination> {

}