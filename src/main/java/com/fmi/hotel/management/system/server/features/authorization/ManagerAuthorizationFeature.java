package com.fmi.hotel.management.system.server.features.authorization;

import com.fmi.hotel.management.system.server.features.core.ManagerFeature;

public class ManagerAuthorizationFeature
{

	private final ManagerFeature managerFeature;

	public ManagerAuthorizationFeature(ManagerFeature managerFeature)
	{
		this.managerFeature = managerFeature;
	}
}
