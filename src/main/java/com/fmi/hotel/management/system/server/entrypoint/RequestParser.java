package com.fmi.hotel.management.system.server.entrypoint;

import com.fmi.hotel.management.system.server.jwt.JwtTokenParserer;

public class RequestParser
{

	private final JwtTokenParserer jwtTokenParserer;

	public RequestParser(JwtTokenParserer jwtTokenParserer)
	{
		this.jwtTokenParserer = jwtTokenParserer;
	}
}
