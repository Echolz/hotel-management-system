package com.fmi.hotel.management.system.server.services;

import com.fmi.hotel.management.system.server.dao.interfaces.CrudDAO;
import com.fmi.hotel.management.system.server.model.Room;

public class RoomsService
{

	private final CrudDAO<Room> roomsDAO;

	public RoomsService(CrudDAO<Room> roomsDAO)
	{
		this.roomsDAO = roomsDAO;
	}
}
