package com.fmi.hotel.management.system.server.model;

import java.time.LocalDate;
import java.util.Objects;


public class Request implements Model {
    private final Integer requestID;
    private final int userID; //or Integer?
    private final Category category;
    private final LocalDate createdOn;
    private final boolean isDone;
    private final String description;

    public Request() {
        requestID = null;
        userID = 0; //-1?
        category = null;
        createdOn = LocalDate.now();
        isDone = false;
        description = new String();
    }

    public Request(Integer requestID, int userID, Category category, LocalDate createdOn, boolean isDone, String description) {
        this.requestID = requestID;
        this.userID = userID;
        this.category = category;
        this.createdOn = createdOn;
        this.isDone = isDone;
        this.description = description;
    }

    public Request(int requestID, Request request) {
        this.requestID = requestID;
        userID = request.userID;
        category = request.category;
        createdOn = request.createdOn;
        isDone = request.isDone;
        description = request.description;
    }

    public Integer getID() {
        return requestID;
    }

    public Category getCategory() {
        return category;
    }

    public boolean isDone() {
        return isDone == true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return userID == request.userID &&
                isDone == request.isDone &&
                category == request.category &&
                Objects.equals(createdOn, request.createdOn) &&
                Objects.equals(description, request.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userID, category, createdOn, isDone, description);
    }
}
