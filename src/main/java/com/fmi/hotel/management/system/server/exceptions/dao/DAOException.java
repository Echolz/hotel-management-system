package com.fmi.hotel.management.system.server.exceptions.dao;

public class DAOException extends Exception {
    private final Class origin;

    public DAOException(String errorMessage, Throwable cause, Class origin) {
        super(errorMessage, cause);
        this.origin = origin;
    }

    public DAOException(String errorMessage, Class origin) {
        super(errorMessage);
        this.origin = origin;
    }

    public Class getOrigin() {
        return origin;
    }
}
