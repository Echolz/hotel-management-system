package com.fmi.hotel.management.system.server.services;


import com.fmi.hotel.management.system.server.dao.interfaces.CrudDAO;
import com.fmi.hotel.management.system.server.model.Feedback;

public class FeedbackService {

    private final CrudDAO<Feedback> feedbackDAO;

    public FeedbackService(CrudDAO<Feedback> feedbackDAO) {
        this.feedbackDAO = feedbackDAO;
    }
}
