package com.fmi.hotel.management.system.server.dao.in_memory;

import com.fmi.hotel.management.system.server.dao.interfaces.FeedbackDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.Feedback;
import com.fmi.hotel.management.system.server.utilities.AutoIncrementer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class InMemoryFeedbackDAO implements FeedbackDAO {

    private final List<Feedback> feedbacks = new ArrayList<>();
    private final AutoIncrementer IDGenerator = new AutoIncrementer();

    private List<Feedback> clone(List<Feedback> source) {
        return source.stream()
                .map(Feedback::new)
                .collect(Collectors.toList());
    }

    private void throwNewDAOException(String message) throws DAOException {
        throw new DAOException(message, InMemoryFeedbackDAO.class);
    }

    @Override
    public Feedback findByID(int ID) {
        Feedback filteredFeedback = feedbacks.stream()
                .filter(f -> f.getID().equals(ID))
                .findFirst()
                .orElse(null);

        return filteredFeedback == null ? null : new Feedback(filteredFeedback);
    }

    @Override
    public List<Feedback> findAll() throws DAOException {
        return clone(feedbacks);
    }

    @Override
    public List<Feedback> findAllByCriteria(Predicate<Feedback> criteria) throws DAOException {
        if (criteria == null) {
            return findAll();
        }

        List<Feedback> filteredFeedbacks = feedbacks.stream()
                .filter(criteria)
                .collect(Collectors.toList());

        return clone(filteredFeedbacks);
    }

    @Override
    public int insert(Feedback feedback) throws DAOException {
        if (feedback == null) {
            throw new DAOException("Cannot insert NULL feedback", this.getClass());
        }

        int generatedID = IDGenerator.generateID();
        feedbacks.add(new Feedback(generatedID, feedback));

        return generatedID;
    }

    @Override
    public void update(Feedback feedback) throws DAOException {
        if (feedback == null) {
            throw new DAOException("Cannot update NULL feedback", this.getClass());
        }
        if (feedback.getID() == null) {
            throw new DAOException("Cannot update feedback with NULL ID", this.getClass());
        }

        boolean wasFeedbackDeleted = feedbacks.removeIf(f -> f.getID().equals(feedback.getID()));

        if (!wasFeedbackDeleted) {
            throwNewDAOException("Feedback entry was not found in the system. " +
                    "Please make sure you're updating an existing record.");
        }

        feedbacks.add(new Feedback(feedback));
    }

    @Override
    public void deleteByID(int ID) throws DAOException {
        boolean wasFeedbackDeleted = feedbacks.removeIf(f -> f.getID().equals(ID));

        if (!wasFeedbackDeleted) {
            throwNewDAOException("Feedback entry was not found in the system. " +
                    "Please make sure you're deleting an existing record.");
        }
    }

    @Override
    public Feedback createModel(int id, Feedback object) {
        return new Feedback(id, object);
    }
}
