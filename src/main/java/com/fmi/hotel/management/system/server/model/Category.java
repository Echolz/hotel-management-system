package com.fmi.hotel.management.system.server.model;

//types of requests
public enum Category {
    MEDICINE,
    REPAIRMENT,
    CLEANING,
    INVENTORY
}