package com.fmi.hotel.management.system.server.dao.interfaces;

import com.fmi.hotel.management.system.server.model.Modelled;
import com.fmi.hotel.management.system.server.model.User;

public interface UsersDAO extends CrudDAO<User>, Modelled<User> {
    //TODO add findByUsername, findByEmail
}