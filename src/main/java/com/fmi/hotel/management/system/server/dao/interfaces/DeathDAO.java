package com.fmi.hotel.management.system.server.dao.interfaces;

import com.fmi.hotel.management.system.server.model.Death;
import com.fmi.hotel.management.system.server.model.Modelled;

public interface DeathDAO extends CrudDAO<Death>, Modelled<Death> {

}