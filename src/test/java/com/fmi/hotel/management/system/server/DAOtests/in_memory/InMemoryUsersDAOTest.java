package com.fmi.hotel.management.system.server.DAOtests.in_memory;

import com.fmi.hotel.management.system.server.DAOtests.GenericDAOTester;
import com.fmi.hotel.management.system.server.dao.in_memory.InMemoryUsersDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.User;
import com.fmi.hotel.management.system.server.model.UserRole;

import java.time.LocalDate;

public class InMemoryUsersDAOTest extends GenericDAOTester<User, InMemoryUsersDAO> {

    @Override
    public InMemoryUsersDAO getDAO() {
        return new InMemoryUsersDAO();
    }

    @Override
    public User makeDummy1() throws DAOException {
        return new User(null, "name1", "pass1", "email1", "firstname1", "lastname1", LocalDate.parse("1990-04-23"), "address1",
                "PID1", UserRole.GUEST);
    }

    @Override
    public User makeDummy2() throws DAOException {
        return new User(null, "name2", "pass2", "email2", "firstname2", "lastname2", LocalDate.parse("1991-04-23"), "address1",
                "PID2", UserRole.ACCOUNTANT);
    }

    @Override
    public User makeDummy3() throws DAOException {
        return new User(null, "name3", "pass3", "email3", "firstname3", "lastname3", LocalDate.parse("1996-04-23"), "address3",
                "PID3", UserRole.WAREHOUSEMAN);
    }

    //TODO add User-specific tests for duplicate checking in emails and usernames
}
