package com.fmi.hotel.management.system.server.DAOtests.in_memory;

import com.fmi.hotel.management.system.server.DAOtests.GenericDAOTester;
import com.fmi.hotel.management.system.server.dao.in_memory.InMemoryFeedbackDAO;
import com.fmi.hotel.management.system.server.model.Feedback;

import java.time.LocalDateTime;

public class InMemoryFeedbackDAOTest extends GenericDAOTester<Feedback, InMemoryFeedbackDAO> {

    @Override
    public InMemoryFeedbackDAO getDAO() {
        return new InMemoryFeedbackDAO();
    }

    @Override
    public Feedback makeDummy1() {
        return new Feedback(null, 10, Feedback.Rating.AVERAGE, "description1", LocalDateTime.parse("2013-05-23T12:00:00"));
    }

    @Override
    public Feedback makeDummy2() {
        return new Feedback(null, 10, Feedback.Rating.EXCELLENT, "description2", LocalDateTime.parse("2013-05-23T18:00:00"));
    }

    @Override
    public Feedback makeDummy3() {
        return new Feedback(null, 10, Feedback.Rating.GOOD, "description3", LocalDateTime.parse("2013-05-23T15:00:00"));
    }
}
