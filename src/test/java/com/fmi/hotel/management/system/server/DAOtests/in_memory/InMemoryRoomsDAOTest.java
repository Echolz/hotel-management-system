package com.fmi.hotel.management.system.server.DAOtests.in_memory;

import com.fmi.hotel.management.system.server.DAOtests.GenericDAOTester;
import com.fmi.hotel.management.system.server.dao.in_memory.InMemoryRoomsDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.Room;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class InMemoryRoomsDAOTest extends GenericDAOTester<Room, InMemoryRoomsDAO> {

    @Override
    public InMemoryRoomsDAO getDAO() {
        return new InMemoryRoomsDAO();
    }

    @Override
    public Room makeDummy1() throws DAOException {
        return new Room(null, 105, 6, 70 * 100, false, true, new HashSet<>(), null);
    }

    @Override
    public Room makeDummy2() throws DAOException {
        return new Room(null, 106, 4, 200 * 100, true, false, new HashSet<>(), null);
    }

    @Override
    public Room makeDummy3() throws DAOException {
        return new Room(null, 107, 4, 100 * 100, true, false, new HashSet<>(), null);
    }

    public Room makeDummyCustom(int roomNumber, Set<Integer> occupants, Integer ownerID) throws DAOException {
        return new Room(null, roomNumber, 6, 70 * 100, false, true, occupants, ownerID);
    }

    @Test @Override
    public void updateNormally() throws DAOException {
        updateNormally(getDAO(), makeDummyCustom(50, new HashSet<Integer>(), null),
                makeDummyCustom(50, new HashSet<Integer>(), 40));
    }

    @Test
    public void createRoomInvalidIDZero() {
        try {
            new Room(0, 1, 1, 1, true, true, new HashSet<Integer>(), 1);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Room ID = 0 is invalid - needs to be positive", e.getMessage());
        }
    }

    @Test
    public void createRoomInvalidIDNegative() {
        try {
            new Room(-6, 1, 1, 1, true, true, new HashSet<Integer>(), 1);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Room ID = -6 is invalid - needs to be positive", e.getMessage());
        }
    }

    @Test
    public void createRoomInvalidNumberZero() {
        try {
            new Room(1, 0, 1, 1, true, true, new HashSet<Integer>(), 1);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Room number = 0 is invalid - needs to be positive", e.getMessage());
        }
    }

    @Test
    public void createRoomInvalidNumberNegative() {
        try {
            new Room(1, -4, 1, 1, true, true, new HashSet<Integer>(), 1);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Room number = -4 is invalid - needs to be positive", e.getMessage());
        }
    }

    @Test
    public void createRoomInvalidBedCountZero() {
        try {
            new Room(null, 107, 0, 100 * 100, true, false, new HashSet<>(), null);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Bed count = 0 is invalid - needs to be positive and no more than 100", e.getMessage());
        }
    }

    @Test
    public void createRoomInvalidBedCountNegative() {
        try {
            new Room(null, 107, -5, 100 * 100, true, false, new HashSet<>(), null);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Bed count = -5 is invalid - needs to be positive and no more than 100", e.getMessage());
        }
    }

    @Test
    public void createRoomInvalidBedCountTooMuch() {
        try {
            new Room(null, 107, 150, 100 * 100, true, false, new HashSet<>(), null);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Bed count = 150 is invalid - needs to be positive and no more than 100", e.getMessage());
        }
    }

    @Test
    public void createRoomInvalidPriceZero() {
        try {
            new Room(null, 107, 50, 0, true, false, new HashSet<>(), null);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Room price = 0 is invalid - needs to be positive", e.getMessage());
        }
    }

    @Test
    public void createRoomInvalidPriceNegative() {
        try {
            new Room(null, 107, 50, -16, true, false, new HashSet<>(), null);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Room price = -16 is invalid - needs to be positive", e.getMessage());
        }
    }

    @Test
    public void createRoomInvalidNullOccupants() {
        try {
            makeDummyCustom(1, null, null);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Cannot have NULL occupants for an owner, use an empty set of IDs instead", e.getMessage());
        }
    }

    @Test
    public void createRoomOccupantsWithoutOwnerInvalid() {
        try {
            HashSet<Integer> occupants = new HashSet<>();
            occupants.add(20);
            makeDummyCustom(1, occupants, null);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Cannot have occupants without owner", e.getMessage());
        }
    }

    @Test
    public void createRoomOccupantsAreNullInvalid() {
        try {
            HashSet<Integer> occupants = new HashSet<>();
            occupants.add(20);
            occupants.add(null);
            makeDummyCustom(1, occupants, 15);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Cannot have NULL ID in the occupants", e.getMessage());
        }

    }

    @Test
    public void createRoomOccupantsContainTheOwnerInvalid() {
        try {
            HashSet<Integer> occupants = new HashSet<>();
            occupants.add(20);
            occupants.add(40);
            makeDummyCustom(1, occupants, 20);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Cannot have the owner as a part of the occupants", e.getMessage());
        }

    }

    @Test
    public void deleteRoomWithExistingGuests() {
        int id = 0;
        try {
            InMemoryRoomsDAO dao = new InMemoryRoomsDAO();
            id = dao.insert(makeDummyCustom(105, new HashSet<Integer>(), 5));
            dao.deleteByID(id);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals(String.format("Can't delete room with ID %d with guests", id), e.getMessage());
        }
    }

    @Test
    public void insertRoomNumberDuplicatesInvalid() {
        try {
            insertNormally(getDAO(), makeDummy1(), makeDummy1(), makeDummy1());
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Can't add duplicate room number 105", e.getMessage());
        }
    }

    @Test
    public void updateRoomChangeTheNumberInvalid() {
        try {
            InMemoryRoomsDAO dao = getDAO();

            Room room1 = makeDummyCustom(5, new HashSet<>(), null);
            Room room2 = makeDummyCustom(6, new HashSet<>(), null);

            int id = dao.insert(room1);
            room2 = dao.createModel(id, room2);

            dao.update(room2);
            Assert.fail("Failure expected");
        } catch (DAOException e) {
            Assert.assertEquals("Can't update the number of room 1 from 5 to 6", e.getMessage());
        }
    }

}
