package com.fmi.hotel.management.system.server.DAOtests.in_memory;

import com.fmi.hotel.management.system.server.DAOtests.GenericDAOTester;
import com.fmi.hotel.management.system.server.dao.in_memory.InMemoryAccidentDAO;
import com.fmi.hotel.management.system.server.model.Accident;

public class InMemoryAccidentDAOTest extends GenericDAOTester<Accident, InMemoryAccidentDAO> {

    @Override
    public InMemoryAccidentDAO getDAO() {
        return new InMemoryAccidentDAO();
    }

    @Override
    public Accident makeDummy1() {
        return new Accident(null, 10, "Mike Hellman", 23, "000000000000", "stable", "aspirin", "common flu", "stay at home");
    }

    @Override
    public Accident makeDummy2() {
        return new Accident(null, 16, "Lydia Simpson", 25, "000000000001", "stable", "", "", "");
    }

    @Override
    public Accident makeDummy3() {
        return new Accident(null, 16, "Maria Vladkova", 25, "000000000002", "stable", "", "", "");
    }
}
