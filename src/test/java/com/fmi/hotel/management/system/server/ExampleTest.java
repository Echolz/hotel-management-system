package com.fmi.hotel.management.system.server;

import org.junit.Assert;
import org.junit.Test;

public class ExampleTest {
    @Test
    public void dividesNormally() {
        int dividend = 20;
        int divisor = 5;

        int expected = 4;
        int actual = dividend / divisor;

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = ArithmeticException.class)
    public void divisionThrowsExceptionWhenDividingByZero() {
        int dividend = 20;
        int divisor = 0;

       int actual = dividend / divisor;
    }

    @Test
    public void alwaysFails() {
        // Assert.assertTrue(false);
    }
}
