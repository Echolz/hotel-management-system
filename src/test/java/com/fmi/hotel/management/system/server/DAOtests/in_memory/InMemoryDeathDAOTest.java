package com.fmi.hotel.management.system.server.DAOtests.in_memory;

import com.fmi.hotel.management.system.server.DAOtests.GenericDAOTester;
import com.fmi.hotel.management.system.server.dao.in_memory.InMemoryDeathDAO;
import com.fmi.hotel.management.system.server.model.Death;

import java.time.LocalDateTime;

public class InMemoryDeathDAOTest extends GenericDAOTester<Death, InMemoryDeathDAO> {
    @Override
    public InMemoryDeathDAO getDAO() {
        return new InMemoryDeathDAO();
    }

    @Override
    public Death makeDummy1() {
        return new Death(null, 10, "Diana Spencer", 34, "alcohol poisoning", LocalDateTime.parse("2017-05-01T16:00:00"), "RANDOM_CERTIFICATE_STRING_1");
    }

    @Override
    public Death makeDummy2() {
        return new Death(null, 12, "Kate Jackson", 40, "gunshot wounds", LocalDateTime.parse("2017-05-01T17:00:00"), "RANDOM_CERTIFICATE_STRING_2");
    }

    @Override
    public Death makeDummy3() {
        return new Death(null, 15, "Peter Bond", 27, "car crash", LocalDateTime.parse("2017-05-01T20:00:00"), "RANDOM_CERTIFICATE_STRING_3");
    }
}
