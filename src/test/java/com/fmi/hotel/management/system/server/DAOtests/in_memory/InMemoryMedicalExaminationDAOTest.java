package com.fmi.hotel.management.system.server.DAOtests.in_memory;

import com.fmi.hotel.management.system.server.DAOtests.GenericDAOTester;
import com.fmi.hotel.management.system.server.dao.in_memory.InMemoryMedicalExaminationDAO;
import com.fmi.hotel.management.system.server.model.MedicalExamination;

public class InMemoryMedicalExaminationDAOTest extends GenericDAOTester<MedicalExamination, InMemoryMedicalExaminationDAO> {
    @Override
    public InMemoryMedicalExaminationDAO getDAO() {
        return new InMemoryMedicalExaminationDAO();
    }

    @Override
    public MedicalExamination makeDummy1() {
        return new MedicalExamination(null, 10, "Steve Bull", 45, "000000000000", "", "", "", "", "");
    }

    @Override
    public MedicalExamination makeDummy2() {
        return new MedicalExamination(null, 11, "Peter Snow", 45, "000000000000", "", "", "", "", "");
    }

    @Override
    public MedicalExamination makeDummy3() {
        return new MedicalExamination(null, 12, "Alex Blake", 45, "000000000000", "", "", "", "", "");
    }
}
