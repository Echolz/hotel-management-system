package com.fmi.hotel.management.system.server.DAOtests.in_memory;

import com.fmi.hotel.management.system.server.DAOtests.GenericDAOTester;
import com.fmi.hotel.management.system.server.dao.in_memory.InMemoryRequestsDAO;
import com.fmi.hotel.management.system.server.model.Category;
import com.fmi.hotel.management.system.server.model.Request;

import java.time.LocalDate;

public class InMemoryRequestDAOTest extends GenericDAOTester<Request, InMemoryRequestsDAO> {

    @Override
    public InMemoryRequestsDAO getDAO() {
        return new InMemoryRequestsDAO();
    }

    @Override
    public Request makeDummy1() {
        return new Request(null, 5, Category.CLEANING, LocalDate.parse("2005-11-26"), false, "The shelves are dusty.");
    }

    @Override
    public Request makeDummy2() {
        return new Request(null, 5, Category.INVENTORY, LocalDate.parse("2005-11-26"), true, "Order 50 kg of potatoes.");
    }

    @Override
    public Request makeDummy3() {
        return new Request(null, 5, Category.REPAIRMENT, LocalDate.parse("2005-11-26"), false, "Room 463's doorknob is broken.");
    }
}