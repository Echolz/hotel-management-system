package com.fmi.hotel.management.system.server.DAOtests.in_memory;

import com.fmi.hotel.management.system.server.DAOtests.GenericDAOTester;
import com.fmi.hotel.management.system.server.dao.in_memory.InMemoryFinancesDAO;
import com.fmi.hotel.management.system.server.model.Payment;

import java.time.LocalDate;

public class InMemoryFinanceDAOTest extends GenericDAOTester<Payment, InMemoryFinancesDAO> {

    @Override
    public InMemoryFinancesDAO getDAO() {
        return new InMemoryFinancesDAO();
    }

    @Override
    public Payment makeDummy1() {
        return new Payment(null, Payment.Type.SALARY, -2000 * 100, "Head Chef Salary", LocalDate.parse("2015-04-21"), 20);
    }

    @Override
    public Payment makeDummy2() {
        return new Payment(null, Payment.Type.ROOM, 70 * 100, "Room 432 fee", LocalDate.parse("2015-03-21"), 20);
    }

    @Override
    public Payment makeDummy3() {
        return new Payment(null, Payment.Type.WAREHOUSE, -1500 * 100, "Electricity bill", LocalDate.parse("2015-07-21"), 20);
    }
}
