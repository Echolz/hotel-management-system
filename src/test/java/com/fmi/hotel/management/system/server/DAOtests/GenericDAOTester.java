package com.fmi.hotel.management.system.server.DAOtests;

import com.fmi.hotel.management.system.server.dao.interfaces.CrudDAO;
import com.fmi.hotel.management.system.server.exceptions.dao.DAOException;
import com.fmi.hotel.management.system.server.model.Model;
import com.fmi.hotel.management.system.server.model.Modelled;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public abstract class GenericDAOTester<DataType extends Model, DaoType extends CrudDAO<DataType> & Modelled<DataType>> {

    abstract public DaoType getDAO();

    abstract public DataType makeDummy1() throws DAOException;

    abstract public DataType makeDummy2() throws DAOException;

    abstract public DataType makeDummy3() throws DAOException;

    @Test
    public void findByIDNormally() throws DAOException {
        findByIDNormally(getDAO(), makeDummy1(), makeDummy2(), makeDummy3());
    }

    public void findByIDNormally(DaoType dao, DataType item1, DataType item2, DataType item3) throws DAOException {

        int ID1 = dao.insert(item1);
        int ID2 = dao.insert(item2);
        int ID3 = dao.insert(item3);

        DataType newItem1 = dao.createModel(ID1, item1);
        DataType newItem2 = dao.createModel(ID2, item2);
        DataType newItem3 = dao.createModel(ID3, item3);

        Assert.assertEquals(dao.findByID(ID1), newItem1);
        Assert.assertEquals(dao.findByID(ID2), newItem2);
        Assert.assertEquals(dao.findByID(ID3), newItem3);
    }


    @Test
    public void findByIDInvalid() throws DAOException {
        findByIDInvalid(getDAO());
    }

    public void findByIDInvalid(DaoType dao) throws DAOException {

        DataType result = dao.findByID(-123);
        Assert.assertNull(result);

        result = dao.findByID(0);
        Assert.assertNull(result);

        result = dao.findByID(1);
        Assert.assertNull(result);

        result = dao.findByID(2);
        Assert.assertNull(result);
    }

    @Test
    public void findAllNormally() throws DAOException {
        findAllNormally(getDAO(), makeDummy1(), makeDummy2(), makeDummy3());
    }

    public void findAllNormally(DaoType dao, DataType item1, DataType item2, DataType item3) throws DAOException {

        DataType newItem1 = dao.createModel(dao.insert(item1), item1);
        DataType newItem2 = dao.createModel(dao.insert(item2), item2);
        DataType newItem3 = dao.createModel(dao.insert(item3), item3);

        List<DataType> items = dao.findAll();

        int counter = 0;
        for (DataType item : items) {
            if (item.equals(newItem1) || item.equals(newItem2) || item.equals(newItem3)) {
                counter++;
            }
        }

        Assert.assertEquals(counter, 3);
    }

    @Test
    public void findAllByCriteriaNormally() throws DAOException {
        findAllByCriteriaNormally(getDAO(), makeDummy1(), makeDummy2(), makeDummy3());
    }

    public void findAllByCriteriaNormally(DaoType dao, DataType item1, DataType item2, DataType item3) throws DAOException {

        dao.insert(item1);
        Integer targetID = dao.insert(item2);
        dao.insert(item3);

        ArrayList<DataType> list = new ArrayList<DataType>(dao.findAllByCriteria(currentItem -> targetID.equals(currentItem.getID())));

        Assert.assertEquals(1, list.size());
        Assert.assertEquals(list.get(0), dao.createModel(targetID, item2));
    }

    @Test
    public void findAllByNullCriteriaNormally() throws DAOException {
        findAllByNullCriteriaNormally(getDAO(), makeDummy1(), makeDummy2(), makeDummy3());
    }

    public void findAllByNullCriteriaNormally(DaoType dao, DataType item1, DataType item2, DataType item3) throws DAOException {

        dao.insert(item1);
        dao.insert(item2);
        dao.insert(item3);

        ArrayList<DataType> list = new ArrayList<DataType>(dao.findAllByCriteria(null));

        Assert.assertEquals(list.size(), 3);

        Assert.assertNotEquals(list.get(0), list.get(1));
        Assert.assertNotEquals(list.get(0), list.get(2));
        Assert.assertNotEquals(list.get(1), list.get(2));

    }

    @Test
    public void insertNormally() throws DAOException {
        insertNormally(getDAO(), makeDummy1(), makeDummy2(), makeDummy3());
    }

    public void insertNormally(DaoType dao, DataType item1, DataType item2, DataType item3) throws DAOException {

        Integer ID1 = dao.insert(item1);
        Integer ID2 = dao.insert(item2);
        Integer ID3 = dao.insert(item3);

        Assert.assertNotNull(ID1);
        Assert.assertNotNull(ID2);
        Assert.assertNotNull(ID3);

        Assert.assertNotEquals(ID1, ID2);
        Assert.assertNotEquals(ID1, ID3);
        Assert.assertNotEquals(ID2, ID3);
    }

    @Test(expected = DAOException.class)
    public void insertNull() throws DAOException {
        insertNull(getDAO());
    }

    public void insertNull(DaoType dao) throws DAOException {
        dao.insert(null);
    }

    @Test
    public void updateNormally() throws DAOException {
        updateNormally(getDAO(), makeDummy1(), makeDummy2());
    }

    public void updateNormally(DaoType dao, DataType item1, DataType item2) throws DAOException {

        int id = dao.insert(item1);

        item2 = dao.createModel(id, item2);

        dao.update(item2);

        Assert.assertEquals(dao.findByID(id), item2);
        Assert.assertNotEquals(dao.findByID(id), item1);
        Assert.assertEquals(dao.findAll().size(), 1);
    }

    @Test(expected = DAOException.class)
    public void updateNull() throws DAOException {
        updateNull(getDAO());
    }

    public void updateNull(DaoType dao) throws DAOException {
        dao.update(null);
    }

    @Test(expected = DAOException.class)
    public void updateNullID() throws DAOException {
        updateNullID(getDAO(), makeDummy1());
    }

    public void updateNullID(DaoType dao, DataType item) throws DAOException {
        dao.insert(item);
        dao.update(item);
    }

    @Test
    public void deleteNormally() throws DAOException {
        deleteNormally(getDAO(), makeDummy1(), makeDummy2(), makeDummy3());
    }

    public void deleteNormally(DaoType dao, DataType item1, DataType item2, DataType item3) throws DAOException {

        ArrayList<DataType> items = new ArrayList<DataType>();

        items.add(item1);
        items.add(item2);
        items.add(item3);

        for(DataType item : items) {
            dao.insert(item);
        }

        ArrayList<DataType> resultItems = new ArrayList<DataType>(dao.findAll());

        for(DataType item : resultItems) {
            dao.deleteByID(item.getID());

            List<DataType> all = dao.findAll();

            Assert.assertEquals(resultItems.size() - 1, all.size());
            Assert.assertTrue("Some of the items are missing", resultItems.containsAll(all));

            dao.insert(item);
        }
    }

    @Test(expected = DAOException.class)
    public void deleteMissing() throws DAOException {
        deleteMissing(getDAO(), makeDummy1(), makeDummy2());
    }

    public void deleteMissing(DaoType dao, DataType item1, DataType item2) throws DAOException {

        int id = dao.insert(item1);
        dao.insert(item2);

        dao.deleteByID(id);
        dao.deleteByID(id);
    }
}